@extends('app')

@section('content')

	<div class="container">

		<hr/>

		<div class="row">
			<h3>Busca</h3>
			<form name="contactForm" action="">
				<input class="form-control" type="text" name="name" placeholder="Nome" />

				<input class="form-control" type="text" name="email" placeholder="Email"/>

				<input class="form-control" type="text"  name="cel_number" placeholder="Número do Telefone" />

				<button class="btn btn-primary">Buscar</button>
			</form>
		</div>

		<div class="row">
			<h4>Usuários</h4>
			<table class="table">
				@foreach ($users as $user)
				<tr>
		    		<td>{{ $user->name }}</td>
		    		<td>{{ $user->email }}</td>
		    		<td>{{ $user->office }}</td>
		    		<td>{{ $user->cel_code }}-{{ $user->cel_number }}</td>
		    		<td>
		    			<a href="/user/edit/{{ $user->id }}">Editar</a> |
		    			<a href="/user/delete/{{ $user->id }}">Deletar</a>
		    		</td>
		    	</tr>
				@endforeach
			</ul>
		</div>
	</div>


@endsection
