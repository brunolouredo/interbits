@extends('app')

@section('content')

    <div class="container">
        <h3>Editar</h3>

        <hr/>

        <div class="row">
            <form name="contactForm" action="/user/update" method="POST">

                <input class="form-control" type="hidden" name="id"  value="{{ $user->id }}" />
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <input class="form-control" type="text" name="name" placeholder="Nome" value="{{ $user->name }}" required/>

                <input class="form-control" type="email" name="email" placeholder="Email" value="{{ $user->email }}" required/>

                <input class="form-control" type="text"  name="cel_number" placeholder="Número do Telefone" value="{{ $user->cel_number }}" required/>

                <select class="form-control" name="office_id">
                    <option>Categoria</option>
                    @foreach ($offices as $office)

                        {{--*/ $selected = '' /*--}}

                        @if ($office->id == $user->id)
                            {{--*/ $selected = 'selected' /*--}}
                        @endif

                        <option value="{{$office->id}}" {{$selected}} >{{$office->name}}</option>
                    @endforeach
                </select>

                <button class="btn btn-primary" ng-click="search(contato)" >Editar</button>
            </form>
        </div>
    </div>


@endsection
