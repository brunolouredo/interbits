<!DOCTYPE html>
<html ng-app="app">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>SAC</title>

	<link href="{{ asset('/css/bootstrap/bootstrap.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/style.css') }}" rel="stylesheet">

</head>
<body ng-controller="AddRegister">

	<div class="container">
		<div class="navbar-header">
			<button type="button" class="collapsed navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-6" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
			<a href="#" class="navbar-brand">Brand</a>
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-6">
			<ul class="nav navbar-nav">
				<li class="active"><a href="/">Home</a></li>
				<li><a href="user/register">Novo Usuário</a></li>
			</ul>
		</div>
	</div>

	@if (Session::has('true_message'))
        <div class="alert">
            <p class="text-center">{{ Session::get('true_message') }}</p>
        </div>
    @endif


	@yield('content')

	<!-- Scripts -->
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
	<script src="js/angular/angular.js"></script>

	<script>
		var app = angular.module('app', []);
		angular.module('app', [], function($interpolateProvider) {
        	$interpolateProvider.startSymbol('<%');
        	$interpolateProvider.endSymbol('%>');
    	});

		angular.module("app").controller("AddRegister", function ($scope, $http) {

		});
	</script>

</body>
</html>

