<html ng-app="app">
<head>
	<meta charset="UTF-8">
	<title>Lista Telefonica</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">


	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
	<script src="js/angular/angular.js"></script>

	<script>
		var app = angular.module('app', []);
		angular.module('app', [], function($interpolateProvider) {
        	$interpolateProvider.startSymbol('<%');
        	$interpolateProvider.endSymbol('%>');
    	});

		angular.module("app").controller("AddRegister", function ($scope, $http) {

		});
	</script>
</head>
<body ng-controller="AddRegister">
	<div class="jumbotron">
		<h3>SAC</h3>

		<hr/>

		<form name="contactForm">
			<input class="form-control" type="text" ng-model="contato.name" name="name" placeholder="Nome" ng-required="true" ng-minlength="10"/>

			<input class="form-control" type="text" ng-model="contato.email" name="email" placeholder="Email" ng-required="true" ng-pattern="/^\d{4,5}-\d{4}$/"/>

			<input class="form-control" type="text" ng-model="contato.order" name="order" placeholder="Número do pedido" ng-required="true" ng-minlength="10"/>

			<input class="form-control" type="text" ng-model="contato.title" name="title" placeholder="Título do pedido" ng-required="true" ng-minlength="10"/>

			<text class="form-control" ng-model="contato.obs" name="obs" placeholder="Observação" ng-required="true" ng-minlength="10"/></text>
		</form>

		<div ng-show="contactForm.name.$dirty" ng-messages="contactForm.name.$error">
			<div ng-message="required" class="alert alert-danger">
				Por favor, preencha o campo nome!
			</div>
			<div ng-message="minlength" class="alert alert-danger">
				O campo nome deve ter no mínimo 10 caracteres.
			</div>
		</div>

		<div ng-show="contactForm.email.$error.required && contactForm.email.$dirty" class="alert alert-danger">
			Por favor, preencha o campo Email!
		</div>
		<div ng-show="contactForm.email.$error.pattern" class="alert alert-danger">
			Email inválido.
		</div>

		<button class="btn btn-primary btn-block" ng-click="adicionarContato(contato)" ng-disabled="contactForm.$invalid">Adicionar Ordem</button>

	</div>

</body>
</html>

