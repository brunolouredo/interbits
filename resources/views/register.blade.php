@extends('app')

@section('content')

    <div class="container">

        <h3>Cadastrar</h3>

        <hr/>

        <div class="row">
            <form name="contactForm" action="/user/save" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <input class="form-control" type="text" name="name" placeholder="Nome" required/>

                <input class="form-control" type="email" name="email" placeholder="Email" required/>

                <input class="form-control" type="password" name="password" placeholder="Senha" required/>

                <input class="form-control" type="text"  name="cel_number" placeholder="Número do Telefone" required/>

                <select class="form-control" name="office_id">
                    <option>Categoria</option>
                    @foreach ($offices as $office)
                        <option value="{{$office->id}}">{{$office->name}}</option>
                    @endforeach
                </select>

                <button class="btn btn-primary">Salvar</button>
            </form>
        </div>

    </div>


@endsection
