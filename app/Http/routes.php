<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', 'UserController@index');
Route::get("/user/edit/{id}", 'UserController@edit');
Route::get("/user/register/", 'UserController@register');

Route::post('/user/save', 'UserController@save');
Route::post('/user/update', 'UserController@update');
Route::get('/user/delete/{id}', 'UserController@delete');

Route::get('/find', function (App\Http\Model\User $User) {

    $post = $_POST;
    $users = $User->get(null, null, $post);

    $return = json_encode($users);

    return $return;
});


Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
