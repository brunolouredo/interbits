<?php

namespace App\Http\Controllers;

// ---------------------------------------------------------------------------->
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\User as User;
use App\Http\Models\Office as Office;

// ---------------------------------------------------------------------------->
/**
 * Controller to user manipulation
 *
 * @package Http
 *
 * @subpackage Controllers
 *
 * @author Bruno Louredo <louredo.bruno@gmail.com>
 *
 * @uses App\Http\Controllers\Controller
 */

// ---------------------------------------------------------------------------->
class UserController extends Controller {

    private $User;

    /**
     * Create a new controller instance and injections.
     *
     * @return void
     */
    public function __construct(User $user, Office $office)
    {
        $this->User = $user;
        $this->Office = $office;
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Return users and offices in database
     *
     * @uses App\Http\Models\User
     */
    public function index ()
    {
        $get = $_GET;

        $user_query = $this->User->join('office', 'office.id', '=', 'user.office_id');
        $user_query->where('active', '1');

        foreach ($get as $name_condition => $value_condition) {
            if ($value_condition) {
                $user_query->where($name_condition, 'LIKE', '%'.$value_condition.'%');
            }
        }

        $users = $user_query->get(['office.name as office', 'office.id as office_id', 'user.*']);

        return view('home', ['users' => $users]);
    }

    /**
     * Edit User Layout.
     *
     * @param int $arg_id_user The user id to find in database
     *
     * @return Return user info to edit
     *
     * @uses App\Http\Models\User
     *
     * @uses App\Http\Models\Office
     */
    public function edit ($arg_id_user)
    {

        $user = $this->User->find($arg_id_user);
        $offices = $this->Office->all();

        return view('edit', ['user' => $user, 'offices' => $offices]);
    }

    /**
     * Register user template.
     *
     * @return Return users and offices in database
     *
     * @uses App\Http\Models\Office
     */
    public function register ()
    {
        $offices = $this->Office->all();

        return view('register', ['offices' => $offices]);
    }

    /**
     * Save a new user in database.
     *
     * @param Obj $request
     *
     * @return null
     *
     * @uses App\Http\Models\User
     *
     * @uses Illuminate\Http\Request
     */
    public function save (Request $request)
    {

        $this->User->name  = $request->name;
        $this->User->email = $request->email;
        $this->User->cel_number  = $request->cel_number;
        $this->User->password = md5($request->password);
        $this->User->office_id  = $request->office_id;

        $save = $this->User->save();

        if ($save) {
            $request->session()->flash('true_message', 'Usuário Salvo!');
        } else {
            $request->session()->flash('error_message', 'Erro ao salvar usuário!');
        }

        return redirect('/');
    }

    /**
     * Update an user in database.
     *
     * @param Obj $request
     *
     * @return null
     *
     * @uses App\Http\Models\User
     *
     * @uses Illuminate\Http\Request
     */
    public function update (Request $request)
    {
        $post = $_POST;
        $id = $post['id'];

        unset($post['id']);
        unset($post['_token']);

        $update = $this->User->where('id', $id)->update($post);

        if ($update) {
            $request->session()->flash('true_message', 'Usuário Salvo!');
        } else {
            $request->session()->flash('error_message', 'Erro ao editar usuário!');
        }

        return redirect('/');
    }

    /**
     * Delete an user in database.
     *
     * @param int $arg_id_user The user id
     *
     * @return null
     *
     * @uses Illuminate\Http\Request
     */
    public function delete ($arg_id_user, Request $request)
    {
        $user = $this->User->where('active', $arg_id_user)->delete();

        $request->session()->flash('true_message', 'Usuário Deletado!');

        return redirect('/');
    }


}
