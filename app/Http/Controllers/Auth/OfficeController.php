<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OfficeController extends Controller {

    private $User;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->User = $user;
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index ()
    {
        $get = $_GET;

        $user_query = $this->User->where('active', '1');

        foreach ($get as $name_condition => $value_condition) {
            if ($value_condition) {
                $user_query->where($name_condition, 'LIKE', '%'.$value_condition.'%');
            }
        }

        $users = $user_query->get();

        return view('home', ['users' => $users]);
    }

    public function edit ($arg_id_user)
    {

        $user = $this->User->find($arg_id_user);

        return view('edit', ['user' => $user]);
    }

    public function register ()
    {
        return view('register');
    }


    public function save (Request $request)
    {

        $this->User->name  = $request->name;
        $this->User->email = $request->email;
        $this->User->cel_number  = $request->cel_number;
        $this->User->password = md5($request->password);

        $save = $this->User->save();

        if ($save) {
            $request->session()->flash('true_message', 'Usuário Salvo!');
        } else {
            $request->session()->flash('error_message', 'Erro ao salvar usuário!');
        }

        return redirect('/');
    }

    public function update (Request $request)
    {
        $post = $_POST;
        $id = $post['id'];

        unset($post['id']);
        unset($post['_token']);

        $update = $this->User->where('id', $id)->update($post);

        if ($update) {
            $request->session()->flash('true_message', 'Usuário Salvo!');
        } else {
            $request->session()->flash('error_message', 'Erro ao editar usuário!');
        }

        return redirect('/');
    }


}
