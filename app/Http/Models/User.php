<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{

    protected $table = 'user';

    public function office ()
    {
        return $this->belongsTo('App\Http\Models\Office');
    }

}
